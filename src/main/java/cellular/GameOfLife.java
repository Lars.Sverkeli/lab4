package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public GameOfLife(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public void specialSetup() {
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				currentGeneration.set(row, col, CellState.DEAD);
			}
		}
		int r = 100;
		int c = 0;
		currentGeneration.set(r + 3, c + 3, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 4, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 5, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 6, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 7, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 8, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 9, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 10, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 12, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 13, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 14, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 15, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 16, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 20, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 21, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 22, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 29, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 30, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 31, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 32, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 33, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 34, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 35, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 37, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 38, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 39, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 40, CellState.ALIVE);
		currentGeneration.set(r + 3, c + 41, CellState.ALIVE);
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				nextGeneration.set(row, col, this.getNextCell(row, col));
			}
		}
		currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		int nLiv = countNeighbors(row, col, CellState.ALIVE);
		if (nLiv == 3) {
			return CellState.ALIVE;
		}
		if ((nLiv == 2) && (currentGeneration.get(row, col) == CellState.ALIVE)) {
			return CellState.ALIVE;
		}
		return CellState.DEAD;
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		int n = 0; 
		for (int i = -1; i < 2; i++){
			for (int j = -1; j < 2; j++){
				if ((i == 0) && (j == 0)) {
					continue;
				}
				if (currentGeneration.get(Math.floorMod(row + i, this.numberOfRows()), Math.floorMod(col + j, this.numberOfColumns())).equals(state)) {
					n ++;
				}
			}
		}
		return n;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
