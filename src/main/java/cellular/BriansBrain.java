package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {
    
        /**
         * The grid of cells
         */
        IGrid currentGeneration;
    
        /**
         * 
         * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
         * provided size
         * 
         * @param height The height of the grid of cells
         * @param width  The width of the grid of cells
         */
        public BriansBrain(int rows, int columns) {
            currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
            initializeCells();
        }
    
        @Override
        public void initializeCells() {
            Random random = new Random();
            for (int row = 0; row < currentGeneration.numRows(); row++) {
                for (int col = 0; col < currentGeneration.numColumns(); col++) {
                    int rand = random.nextInt(3);
                    if (rand == 0) {
                        currentGeneration.set(row, col, CellState.ALIVE);
                    } else if (rand == 1) {
                        currentGeneration.set(row, col, CellState.DYING);
                    } else {
                        currentGeneration.set(row, col, CellState.DEAD);
                    }
                }
            }
        }

        @Override
        public void specialSetup() {
            for (int row = 0; row < currentGeneration.numRows(); row++) {
                for (int col = 0; col < currentGeneration.numColumns(); col++) {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
            int r = 50;
            int c = 50;
            currentGeneration.set(r + 0, c + 0, CellState.ALIVE);
            currentGeneration.set(r + 1, c + 0, CellState.ALIVE);
            currentGeneration.set(r + 0, c + 1, CellState.ALIVE);
            currentGeneration.set(r + 1, c + 1, CellState.ALIVE);
        }
    
        @Override
        public int numberOfRows() {
            return currentGeneration.numRows();
        }
    
        @Override
        public int numberOfColumns() {
            return currentGeneration.numColumns();
        }
    
        @Override
        public CellState getCellState(int row, int col) {
            return currentGeneration.get(row, col);
        }
    
        @Override
        public void step() {
            IGrid nextGeneration = currentGeneration.copy();
            for (int row = 0; row < currentGeneration.numRows(); row++) {
                for (int col = 0; col < currentGeneration.numColumns(); col++) {
                    nextGeneration.set(row, col, this.getNextCell(row, col));
                }
            }
            currentGeneration = nextGeneration;
        }
    
        @Override
        public CellState getNextCell(int row, int col) {
            int nLiv = countNeighbors(row, col, CellState.ALIVE);

            if (currentGeneration.get(row, col) == CellState.ALIVE) {
                return CellState.DYING;
            }
            if ((nLiv == 2) && (currentGeneration.get(row, col) == CellState.DEAD)) {
                return CellState.ALIVE;
            }
            return CellState.DEAD;
        }
    
        /**
         * Calculates the number of neighbors having a given CellState of a cell on
         * position (row, col) on the board
         * 
         * Note that a cell has 8 neighbors in total, of which any number between 0 and
         * 8 can be the given CellState. The exception are cells along the boarders of
         * the board: these cells have anywhere between 3 neighbors (in the case of a
         * corner-cell) and 5 neighbors in total.
         * 
         * @param x     the x-position of the cell
         * @param y     the y-position of the cell
         * @param state the Cellstate we want to count occurences of.
         * @return the number of neighbors with given state
         */
        private int countNeighbors(int row, int col, CellState state) {
            int n = 0; 
            for (int i = -1; i < 2; i++){
                for (int j = -1; j < 2; j++){
                    if ((i == 0) && (j == 0)) {
                        continue;
                    }
                    if (currentGeneration.get(Math.floorMod(row + i, this.numberOfRows()), Math.floorMod(col + j, this.numberOfColumns())).equals(state)) {
                        n ++;
                    }
                }
            }
            return n;
        }
    
        @Override
        public IGrid getGrid() {
            return currentGeneration;
        }
    }
    
    
