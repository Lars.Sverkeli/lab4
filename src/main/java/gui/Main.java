package gui;

import cellular.CellAutomaton;
import cellular.GameOfLife;
import cellular.BriansBrain;
import cellular.Seeds;

public class Main {

	public static void main(String[] args) {
		CellAutomaton ca = new GameOfLife(300, 300);
//		CellAutomaton ca = new BriansBrain(200, 200);
//		CellAutomaton ca = new Seeds(300, 200);
		CellAutomataGUI.run(ca);
	}

}
