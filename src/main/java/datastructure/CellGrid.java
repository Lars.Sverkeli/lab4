package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

private int rows;
private int columns;
private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.columns = columns;
        this.grid = new CellState[rows][columns];
        for(int i = 0; i < this.rows; i++) {
            for(int j = 0; j < this.columns; j++) {
                this.grid[i][j] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        this.grid[row][column] = element;        
    }

    @Override
    public CellState get(int row, int column) {
        return this.grid[row][column];
    }

    @Override
    public CellGrid copy() {
        CellGrid newGrid = new CellGrid(this.rows, this.columns, CellState.DEAD);  
            for (int i = 0; i < this.rows; i++) {
                for(int j = 0; j < this.columns; j++) {
                    newGrid.set(i, j, this.grid[i][j]);
                }
            }
        return newGrid;
    }
}
